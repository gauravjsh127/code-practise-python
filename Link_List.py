class Node:
    def __init__(self,initdata):
        self.data = initdata
        self.next = None
    def getData(self):
        return self.data
    def getNext(self):
        return self.next
    def setData(self,newdata):
        self.data = newdata
    def setNext(self,newnext):
        self.next = newnext

class MyList:
	def __init__(self):
	    self.root = None
	    self.head = None
	
	def add(self,item):
		temp = Node(item)
		temp.setNext(None)
		if self.root == None:
			self.root=temp
			self.head=temp
	
		self.head.setNext(temp)
		self.head = temp
	
		
	def size(self):
	    current = self.root
	    count = 0
	    while current != None:
	        count = count + 1
	        current = current.getNext()
	    return count
		
	def reverse(self):
		prev = None
		current = self.root
		next = None
		while current != None: 
			next=current.getNext()
			current.setNext(prev)
			prev=current
			current=next
		self.root =prev	

		
	def show(self):
		current = self.root
		list=[]
		while current != None: 
			list.append(current.getData())
			current = current.getNext()
		
		print list
		
	def search(self,item):
	    current = self.root
	    found = False
	    while current != None and not found:
	        if current.getData() == item:
	            found = True
	        else:
	            current = current.getNext()
	
	    return found
	
	def xyz(self):
		current = self.root
		list=[]
		while current != None: 
			list.append(current.getData())
			current = current.getNext()
		
		print list
		
	def remove(self,item):
	    current = self.root
	    previous = None
	    found = False
	    while not found:
	        if current.getData() == item:
	            found = True
	        else:
	            previous = current
	            current = current.getNext()
	
	    if previous == None:
	        self.root = current.getNext()
	    else:
	        previous.setNext(current.getNext())


mylist = MyList()

mylist.add(31)
mylist.add(77)
mylist.add(17)
mylist.add(93)
mylist.add(26)
mylist.add(54)

mylist.show()

mylist.reverse()

mylist.show()



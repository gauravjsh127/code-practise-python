class swap:
                def __init__(self,a,b):
                                self.a=a
                                self.b=b
                                print 'Initial '
                                print 'self.a '+ str(self.a)
                                print 'self.b '+ str(self.b)
 
                def swap_basic(self):
                                c=self.b
                                self.b=self.a
                                self.a=c
                                print 'self.a '+ str(self.a)
                                print 'self.b '+ str(self.b)
                                                               
                def swap_advance(self):
                                self.a=self.a+self.b
                                self.b=self.a-self.b
                                self.a=self.a-self.b
                                print 'self.a '+ str(self.a)
                                print 'self.b '+ str(self.b)
                               
p=swap(2,8)
p.swap_advance()
p.swap_basic()